import numpy as np

NEGATIVE = 0
POSITIVE = 1


class Perceptron:
    def __init__(self, n, max_iter=10_000):
        self.w = np.ones(n + 1)
        self.max_iter = max_iter

    def train(self, data_x, data_y):
        error = True
        iter = 0
        while error and iter <= self.max_iter:
            error = False
            iter += 1
            for (x, y) in zip(data_x, data_y):
                res = self.predict(x)
                if res != y:
                    error = True
                    new_x = np.append(np.array([1]), x)
                    self.w += new_x if res == NEGATIVE and y == POSITIVE \
                        else -new_x

    def predict(self, x):
        x = np.append(np.array([1]), x)
        return POSITIVE if np.dot(self.w, x) > 0 else NEGATIVE


if __name__ == "__main__":
    n = 2
    x = np.array([[0, 1], [1, 2], [2, 2.5],
                  [1, 0], [2, 1], [2.5, 2]])

    y = np.array([0, 0, 0, 1, 1, 1])

    p = Perceptron(n)
    p.train(x, y)
    print('Found', p.w)

    print('Testing:')
    print('[3, 2] is', p.predict(np.array([3, 2])), 'should be 1')
    print('[2, 3] is', p.predict(np.array([2, 3])), 'should be 0')
    print('[0, 0] is', p.predict(np.array([0, 0])), 'should be 0')
